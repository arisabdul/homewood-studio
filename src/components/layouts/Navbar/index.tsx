import { useEffect } from "react";
import Link from "next/link";

export default function Navbar() {
  useEffect(() => {
    setNav();
  }, []);

  function setNav() {
    const handleScroll = () => {
      const navbar = document.getElementById("navbar");
      if (navbar) {
        if (window.scrollY > 0) {
          navbar.classList.add("bg-brand-black");
          navbar.classList.remove("bg-transparent");
        } else {
          navbar.classList.remove("bg-brand-black");
          navbar.classList.add("bg-transparent");
        }
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }

  return (
    <>
      <nav id="navbar" className="bg-transparent p-4">
        <div className="nav">
          <div className="left-bar">
            <Link href="/" className="active">
              Home
            </Link>
            <Link href="/">About</Link>
            <Link href="/">Services</Link>
          </div>

          <div className="center-bar">
            <Link href={"/"}>
              <img
                src="/images/logo.png"
                alt="Logo"
                className="  lg:h-6 w-fit"
              />
            </Link>
          </div>

          <div className="right-bar">
            <Link href={"/"} className="btn btn-lime hidden lg:flex">
              Contact Us
            </Link>
            <button className="flex lg:hidden">
              <svg
                className="w-[24px] stroke-brand-light"
                fill="none"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 6h16M4 12h16M4 18h16M4 6h7"
                />
              </svg>
            </button>
          </div>
        </div>
      </nav>
    </>
  );
}
