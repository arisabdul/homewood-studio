import Link from "next/link";

export default function Home() {
  return (
    <main>
      <section id="jumbotron">
        <div className="jumbotron-container">
          <div className="left-jumbotron">
            <h1 className="slogan">
              Furnishing Dreams,{" "}
              <span className="text-primary-blue">
                Crafting Comfort - Your Home, Our Passion
              </span>
            </h1>
          </div>
          <div className="right-jumbotron ">
            <div className="bg-video">
              <img
                src="../../../images/jumbotron/chair.jpg"
                alt="Example Image"
                className="video"
              />
            </div>
          </div>
        </div>
        <div className="jumbotron-about">
          <p className="text-brand-light">
            We are a company dedicated to preserving the beauty and elegance of
            wood in every product. Our design style is inspired by the vibrant
            era, with unique accents that evoke memories and build a connection
            between functionality and aesthetic beauty. Stay up to date with us
            and get great news from us
          </p>
          <form action="#" className="w-full lg:w-1/2 mt-6 inline-form">
            <input
              type="text"
              name="email"
              id="input-email"
              className="form-input"
              placeholder="Fill Your Email Address"
            />
            <input
              type="submit"
              value="Send"
              className="btn btn-border-light form-input"
            />
          </form>
        </div>
      </section>

      <section id="product-best">
        <div className="product-best-container">
          <h4>
            Our Product <span>Best Seller</span>
          </h4>
          <p className="description-section">
            From ergonomic chairs to innovative wardrobes, our furniture
            collection combines the clarity of form and boldness of design that
            was characteristic of the 80s.
          </p>

          <div className="list-product-best-seller">
            <div className="product-best-seller">
              <Link href={"/"} className="img-product">
                <img src="/images/product/product-3.jpg" alt="product-3" />
              </Link>
              <Link href={"/"} className="deskripsi-best-product">
                <p>
                  <span>Living Room Mahogany Chair</span>/ stock 20 / $200
                </p>
              </Link>
            </div>
            <div className="product-best-seller">
              <Link href={"/"} className="img-product">
                <img src="/images/product/product-2.jpg" alt="product-2" />
              </Link>
              <Link href={"/"} className="deskripsi-best-product">
                <p>
                  <span>Cupboard Mini Classic</span>/ stock 12 / $420
                </p>
              </Link>
            </div>
            <div className="product-best-seller">
              <Link href={"/"} className="img-product">
                <img src="/images/product/product-1.jpg" alt="product-1" />
              </Link>
              <Link href={"/"} className="deskripsi-best-product">
                <p>
                  <span>Cafe Chair and Table set</span>/ stock 3 / $890
                </p>
              </Link>
            </div>
          </div>

          <Link href={"/"} className="btn btn-lime mt-12 lg:mt-20">
            See More Our Product
          </Link>
        </div>
      </section>

      <section id="vision">
        <div className="vision-container">
          <h1>
            Expertise and <span>Quality</span>
          </h1>
          <p>
            We are a community of woodworkers committed to delivering the
            highest craftsmanship in every detail of our products. Each piece of
            wood is carefully selected and handcrafted to create a product
            unmatched in quality and durability.
          </p>
        </div>
      </section>

      <section id="testimoni">
        <div className="testimoni-container">
          <h4>
            Testimonial <span>our customers</span>
          </h4>
          <p className="description-section">
            We already have several regular customers from more than 50
            companies and partners throughout the world
          </p>

          <div className="list-testimoni">
            <div className="profile-person">
              <img src="/images/testimoni/person-1.jpg" alt="person-1" />
              <h5 className="mt-6">Anna Peter</h5>
              <p>Ceo of Coffee Corner</p>
            </div>
            <div className="quote-person">
              <h5>
                “I am very happy with my furniture purchase from Wood Home
                Studio. The quality of the wood they use is incredible, and the
                design is very much to my taste. “
              </h5>
              <p>20 / 10 / 2023 ~</p>
            </div>
          </div>
        </div>
      </section>

      <section id="footer">
        <div className="footer-1-container">
          <div className="left-bar">
            <p className="text-brand-light">
              If you have questions or suggestions, we will be happy to respond
              to your message
            </p>
            <form action="#" className="w-full mt-6 inline-form">
              <input
                type="text"
                name="email"
                id="input-email"
                className="form-input"
                placeholder="Fill Your Email Address"
              />
              <input
                type="submit"
                value="Send"
                className="btn btn-border-light form-input"
              />
            </form>
          </div>
          <div className="right-bar">
            <h1 className="slogan text-right text-primary-lime">
              Transforming Spaces{" "}
              <span className="text-primary-blue">
                Elevating Lives Where Style Meets Serenity
              </span>
            </h1>
          </div>
        </div>

        <div className="footer-2-container">
          <div className="left-bar">
            <Link href={"/"}>
              <img
                src="/images/logo.png"
                alt="Logo"
                className="h-5 lg:h-6 w-fit"
              />
            </Link>
          </div>
          <div className="right-bar">
            <Link href="/" className="text-brand-light">
              Home
            </Link>
            <Link href="/" className="text-brand-light">
              About
            </Link>
            <Link href="/" className="text-brand-light">
              Services
            </Link>
          </div>
        </div>
      </section>
    </main>
  );
}
