import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    colors: {
      "primary-lime": "#9dfe03",
      "secondary-lime": "#d3ecac",
      "primary-blue": "#00FFF0",
      "brand-black": "#050503",
      "brand-light": "#e7e9ea",
    },
    extend: {
      screens: {
        sm: "576px",
        md: "768px",
        lg: "992px",
        xl: "1200px",
        "2xl": "1400px",
        "3xl": "1600px",
        "4xl": "1920px",
      },
      container: {
        center: true,
      },
      backgroundImage: {
        "jumbotron-image": 'url("/images/jumbotron/bg.jpg")',
        "vision-image": 'url("/images/vision/bg.jpg")',
        "footer-image": 'url("/images/footer/bg.jpg")',
      },
    },
  },
  plugins: [
    function ({ addComponents }: { addComponents: Function }) {
      addComponents({
        ".container": {
          maxWidth: "100%",
          "@screen sm": {
            maxWidth: "540px",
          },
          "@screen md": {
            maxWidth: "720px",
          },
          "@screen lg": {
            maxWidth: "960px",
          },
          "@screen xl": {
            maxWidth: "1140px",
          },
          "@screen 2xl": {
            maxWidth: "1280px",
          },
          "@screen 3xl": {
            maxWidth: "1320px",
          },
          "@screen 4xl": {
            maxWidth: "1640px",
          },
        },
      });
    },
  ],
};
export default config;
